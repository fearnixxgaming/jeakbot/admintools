package de.fearnixx.t3.admintools.controllers.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.fearnixx.jeak.teamspeak.data.IClient;

public class ClientEntity {

    public static ClientEntity fromData(IClient client) {
        ClientEntity entity = new ClientEntity();
        entity.id = client.getClientID();
        entity.dbId = client.getClientDBID();
        entity.uniqueId = client.getClientUniqueID();
        entity.subjectId = client.getUniqueID().toString();
        entity.nickname = client.getNickName();
        entity.description = client.getDescription();
        entity.platform = client.getPlatform().toString();
        entity.version = client.getVersion();
        entity.type = client.getClientType().toString();
        entity.countryCode = client.getCountryCode();
        entity.channelId = client.getChannelID();
        entity.isAway = client.isAway();
        entity.hasMicrophone = client.hasMic();
        entity.hasMicrophoneMuted = client.hasMicMuted();
        entity.hasSpeakers = client.hasOutput();
        entity.hasSpeakersMuted = client.hasOutputMuted();
        return entity;
    }

    @JsonProperty
    private int id;

    @JsonProperty
    private int dbId;

    @JsonProperty
    private String uniqueId;

    @JsonProperty
    private String subjectId;

    @JsonProperty
    private String nickname;

    @JsonProperty
    private String description;

    @JsonProperty
    private String platform;

    @JsonProperty
    private String version;

    @JsonProperty
    private String type;

    @JsonProperty
    private String countryCode;

    @JsonProperty
    private int channelId;

    @JsonProperty
    private boolean isAway;

    @JsonProperty
    private boolean hasMicrophone;

    @JsonProperty
    private boolean hasMicrophoneMuted;

    @JsonProperty
    private boolean hasSpeakers;

    @JsonProperty
    private boolean hasSpeakersMuted;
}
