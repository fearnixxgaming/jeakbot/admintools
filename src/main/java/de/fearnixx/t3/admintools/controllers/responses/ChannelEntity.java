package de.fearnixx.t3.admintools.controllers.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.fearnixx.jeak.teamspeak.data.IChannel;

public class ChannelEntity {

    public static ChannelEntity fromData(IChannel channel) {
        ChannelEntity entity = new ChannelEntity();
        entity.channelId = channel.getID();
        entity.channelName = channel.getName();
        entity.channelTopic = channel.getTopic();
        entity.isSpacer = channel.isSpacer();
        entity.isDefault = channel.isDefault();
        entity.clientCount = channel.getClientCount();
        entity.familyClientCount = channel.getClientCountBelow();
        entity.maxClients = channel.getMaxClientCount();
        entity.familyMaxClients = channel.getMaxClientCountBelow();
        entity.codecId = channel.getCodec();
        entity.codecQuality = channel.getCodecQuality();
        entity.orderId = channel.getOrder();
        entity.parentId = channel.getParent();
        entity.requiredTalkPower = channel.getTalkPower();
        return entity;
    }

    @JsonProperty
    private int channelId;

    @JsonProperty
    private String channelName;

    @JsonProperty
    private String channelTopic;

    @JsonProperty
    private boolean isSpacer;

    @JsonProperty
    private boolean isDefault;

    @JsonProperty
    private int clientCount;

    @JsonProperty
    private int familyClientCount;

    @JsonProperty
    private int maxClients;

    @JsonProperty
    private int familyMaxClients;

    @JsonProperty
    private int codecId;

    @JsonProperty
    private int codecQuality;

    @JsonProperty
    private int orderId;

    @JsonProperty
    private int parentId;

    @JsonProperty
    private int requiredTalkPower;
}
